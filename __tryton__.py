# -*- coding: utf-8 -*-
# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Name to specify for purchase_text_module',
    # 'name_de_DE': '',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Description to specify for purchase_text_module
    ''',
    # 'description_de_DE': '''
    #''',
    'depends': [
        'account_invoice_text_module',
        'purchase',
    ],
    'xml': [
        'purchase.xml',
    ],
    'translation': [
        # 'locale/de_DE.po',
    ],
}
